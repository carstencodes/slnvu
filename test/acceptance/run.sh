#!/bin/sh

set -e

CURDIR=$CI_PROJECT_DIR/test/acceptance/

export BINDIR=$CI_PROJECT_DIR/src/bin/Release

for script in ${CURDIR}/run/*.sh;
do
  echo Executing acceptance test script ${script} using compare script "${CURDIR}/compare.sh"
  . ${script} "${CURDIR}/compare.sh"
  echo Execution of script finished with status $?
done
#!/bin/sh

set -e

if [ -z "$1" ]
then
  echo "No original file set"  >&2
  return 1
fi

if [ -z "$2" ]
then
  echo "No file to compare set"  >&2
  return 2
fi

if [ ! -f "$1" ]
then
  echo "Original file $1 does not exist"  >&2
  return 11
fi

if [ ! -f "$2" ]
then
  echo "File to compare $2 does not exist"  >&2
  return 12
fi

if [ ! -r "$1" ]
then
  echo "Cannot read file $1"  >&2
  return 21
fi

if [ ! -r "$2" ]
then
  echo "Cannot read file $2"  >&2
  return 22
fi

diff -s "$1" "$2"
if [ $? -eq 0 ]
then
  echo "### SUCCESS : $1 and $2 are equal     ###"
else
  echo "### FAIL    : $1 and $2 are not equal ###" >&2
  diff $1 $2 >&2
  return 31
fi

return 0
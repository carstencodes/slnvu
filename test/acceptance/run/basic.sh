#!/bin/sh

set -e

RUNDIR=$CI_PROJECT_DIR/test/acceptance/run/
SRCDIR=$CI_PROJECT_DIR/src

dotnet $BINDIR/slnvu.dll ${SRCDIR}/src.sln > $RUNDIR/basic.current.log
sed "s%###PATH###%${CI_PROJECT_DIR}%g" < ${RUNDIR}/basic.log > ${RUNDIR}/basic.original.log

. $1 "$RUNDIR/basic.original.log" "$RUNDIR/basic.current.log"

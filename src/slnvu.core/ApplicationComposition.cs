using System;
using slnvu.core.Configuration;
using slnvu.core.Factory;
using slnvu.core.Logging;

namespace slnvu.core
{
    public sealed class ApplicationComposition : IApplicationComposition
    {

        public ApplicationComposition()
        {
            this.Factory = new AutofacObjectFactory(this.Log, this.Configuration);
        }

        public IApplicationConfiguration Configuration { get; } = new InvisionwareSettingsConfiguration();

        public IObjectFactory Factory { get; }

        public ILogger Log { get; } = new SerilogLogger();
    }
}

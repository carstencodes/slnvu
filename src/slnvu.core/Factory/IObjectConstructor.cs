namespace slnvu.core.Factory
{
    public interface IObjectConstructor
    {
        void AnnounceAvailableObjects(IObjectRegistry registry);
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Autofac;
using slnvu.core.Configuration;
using slnvu.core.Logging;

namespace slnvu.core.Factory
{
    public class AutofacObjectFactory : IObjectFactory, IObjectRegistry, IStringParameterizedObjectFactory
    {
        private readonly ContainerBuilder builder = new ContainerBuilder();
        private readonly ILogger logger;
        private readonly IApplicationConfiguration configuration;

        private readonly IDictionary<Type, (string, string)[]> registeredKeysPerType = new Dictionary<Type, (string, string)[]>();

        private IContainer container = new ContainerBuilder().Build();

        public AutofacObjectFactory(ILogger logger, IApplicationConfiguration configuration)
        {
            this.logger = logger;
            this.configuration = configuration;
        }

        public void BeginInit()
        {
        }

        public T Create<T>() where T : class
        {
            T instance = this.container.Resolve<T>();

            return instance ?? throw new InvalidOperationException($"There is no instance for the contract {typeof(T).FullName} registered.");
        }

        public T Create<T>(string key)
        {
            return this.container.ResolveKeyed<T>(key);
        }

        public void EndInit()
        {
            this.builder.RegisterInstance<ILogger>(this.logger).As<ILogger>().IfNotRegistered(typeof(ILogger)).SingleInstance();
            this.builder.RegisterInstance<IApplicationConfiguration>(this.configuration).IfNotRegistered(typeof(IApplicationConfiguration)).SingleInstance();
            this.builder.RegisterInstance<IStringParameterizedObjectFactory>(this).IfNotRegistered(typeof(IStringParameterizedObjectFactory)).SingleInstance();
            this.container = this.builder.Build();
        }

        public string GetDescriptionForKey(string key)
        {
            foreach ((string, string)[] array in this.registeredKeysPerType.Values)
            {
                foreach ((string, string) item in array)
                {
                    string storedKey, description;
                    (storedKey, description) = item;
                    if (StringComparer.InvariantCulture.Equals(key, storedKey))
                    {
                        return description;
                    }
                }
            }

            return string.Empty;
        }

        public string[] GetKeys<T>()
        {
            if (this.registeredKeysPerType.TryGetValue(typeof(T), out (string key, string description)[] keys))
            {
                return keys.Select(item => item.key).ToArray();
            }

            return Array.Empty<string>();
        }

        public void RegisterObjectsFromAssembly<TInterface>() where TInterface : class
        {
            Assembly callingAssembly = Assembly.GetCallingAssembly();
            IEnumerable<Type> types = callingAssembly.GetTypes().Where(type => type.IsAssignableTo<TInterface>());
            types = types.Where(type => type.IsClass && !type.IsAbstract);
            types = types.Where(type => type.GetCustomAttributes<StringMetaDataAttribute>().Any());

            List<(string, string)> registeredKeys = new List<(string, string)>();

            foreach (Type type in types)
            {
                IEnumerable<StringMetaDataAttribute> metaData = type.GetCustomAttributes<StringMetaDataAttribute>();
                foreach (StringMetaDataAttribute item in metaData)
                {
                    this.builder.RegisterType(type).As<TInterface>().AsImplementedInterfaces().Keyed<TInterface>(item.Value);
                    registeredKeys.Add((item.Value, item.Description));
                }
            }

            this.registeredKeysPerType.Add(typeof(TInterface), registeredKeys.ToArray());
        }

        void IObjectRegistry.RegisterObject<TInterface, TInstance>()
        {
            this.builder.RegisterType<TInstance>().As<TInterface>();
        }
    }
}

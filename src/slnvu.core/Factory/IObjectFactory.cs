namespace slnvu.core.Factory
{
    public interface IObjectFactory
    {
        T Create<T>() where T : class;
    }
}

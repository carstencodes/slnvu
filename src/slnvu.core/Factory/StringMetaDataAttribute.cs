using System;

namespace slnvu.core.Factory
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
    public sealed class StringMetaDataAttribute : Attribute
    {
        public StringMetaDataAttribute(string value)
        {
            this.Value = value;
        }

        public string Value { get; }

        public string Description { get; set; } = string.Empty;
    }
}

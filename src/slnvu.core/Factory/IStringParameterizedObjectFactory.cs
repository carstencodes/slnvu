namespace slnvu.core.Factory
{
    public interface IStringParameterizedObjectFactory : IObjectFactory
    {
        T Create<T>(string key);

        string[] GetKeys<T>();
        string GetDescriptionForKey(string key);
    }
}

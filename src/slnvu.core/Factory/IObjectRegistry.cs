using System.ComponentModel;

namespace slnvu.core.Factory
{
    public interface IObjectRegistry : ISupportInitialize
    {
        void RegisterObject<TInterface, TInstance>() 
            where TInterface : class
            where TInstance : class, TInterface;

        void RegisterObjectsFromAssembly<TInterface>()
            where TInterface : class;
    }
}

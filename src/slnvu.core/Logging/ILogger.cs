namespace slnvu.core.Logging
{
    using System;

    public interface ILogger
    {
        void Debug(string formattedMessage, params object[] args);

        void Info(string formattedMessage, params object[] args);

        void Warning(string formattedMessage, params object[] args);

        void Error(string formattedMessage, params object[] args);

        void HandleException(Exception exception); 
    }
}

namespace slnvu.core.Logging
{
    using System;

    using ISerilogLogger = Serilog.ILogger;
    using Serilog;
    using Serilog.Exceptions;

    public sealed class SerilogLogger : ILogger
    {
        private readonly ISerilogLogger logger = new LoggerConfiguration().Enrich.WithExceptionDetails().WriteTo.Console().CreateLogger();


        public void Debug(string formattedMessage, params object[] arguments)
        {
            this.logger.Debug(formattedMessage, arguments);
        }

        public void Info(string formattedMessage, params object[] arguments)
        {
            this.logger.Information(formattedMessage, arguments);
        }

        public void Warning(string formattedMessage, params object[] arguments)
        {
            this.logger.Warning(formattedMessage, arguments);   
        }

        public void Error(string formattedMessage, params object[] arguments)
        {
            this.logger.Error(formattedMessage, arguments);
        }

        public void HandleException(Exception exception)
        {
            this.logger.Fatal("An error occurred while processing the command: {exception}", exception);
        }
    }
}

using slnvu.core.Configuration;
using slnvu.core.Factory;
using slnvu.core.Logging;

namespace slnvu.core
{
    public interface IApplicationComposition
    {
        IApplicationConfiguration Configuration { get; }

        IObjectFactory Factory { get; }

        ILogger Log { get; }
    }
}

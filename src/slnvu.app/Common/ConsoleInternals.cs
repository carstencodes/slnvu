namespace slnvu.app.Common
{
    internal static class ConsoleInternals
    {
        private const string Hyphen = "-";

        internal const string StandardInputAsFile = Hyphen;

        internal const string StandardOutputAsFile = Hyphen;
    }
}

using System;
using System.IO;
using slnvu.app.Arguments;
using slnvu.app.Common;
using slnvu.app.IO;
using slnvu.core.Logging;
using slnvu.engine;

namespace slnvu.app
{
    public sealed class ConsoleApplication : IApplication
    {
        private const int ExitSuccess = 0;

        private const int ExitCommandLineParsingFailure = 16;

        private const int ExitGenericFailure = 1;

        private readonly IArgumentParser argumentParser;

        private readonly ISolutionViewEngine solutionViewEngine;

        private readonly IStreamFactory streamFactory;

        private readonly ILogger logger;

        public ConsoleApplication(IArgumentParser argumentParser, IStreamFactory streamFactory, ISolutionViewEngine solutionViewEngine, ILogger logger)
        {
            this.argumentParser = argumentParser;
            this.streamFactory = streamFactory;
            this.solutionViewEngine = solutionViewEngine;
            this.logger = logger;
        }

        public int Run(string[] arguments)
        {
            try 
            {
                IArguments commands = this.argumentParser.Parse(arguments);

                if (null != commands)
                {
                    return this.ExportSolution(commands);
                }

                return ExitCommandLineParsingFailure;
            }
            catch (Exception e)
            {
                this.logger?.HandleException(e);
                return ExitGenericFailure;
            }
        }

        private int ExportSolution(IArguments arguments)
        {
            string format = arguments.ExportFormat;
                            
            using (Stream input = this.streamFactory.OpenInputStream(arguments.InputFile))
            {
                using (Stream output = this.streamFactory.OpenOutputStream(arguments.OutputFile))
                {
                    this.solutionViewEngine.ExportSolution(input, format, output);
                }
            }

            return ExitSuccess;
        }
    }
}

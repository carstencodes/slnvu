namespace slnvu.app
{
    public interface IApplication
    {
        int Run(string[] arguments); 
    }
}

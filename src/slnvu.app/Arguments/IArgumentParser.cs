namespace slnvu.app.Arguments
{
    public interface IArgumentParser
    {
         IArguments Parse(string[] arguments);
    }
}

using NClap.Metadata;
using slnvu.app.Common;

namespace slnvu.app.Arguments
{
    internal sealed class NClapArguments : IArguments
    {
        [PositionalArgument(ArgumentFlags.AtMostOnce, Position = 0, DefaultValue = ConsoleInternals.StandardInputAsFile, Description = "Name of the input file. If omitted, StdIn will be used. This can be enforced by using '-' as input file.")]
        public string InputFile { get; set; }

        [NamedArgument(ArgumentFlags.Optional, LongName = "output", ShortName = "o", DefaultValue = ConsoleInternals.StandardOutputAsFile, Description = "Name of the output file. StdOut will be used, if omitted. This can be enforced by using '-' as output file.")]
        public string OutputFile { get; set; }

        [NamedArgument(ArgumentFlags.Optional, LongName = "format", ShortName = "f", DefaultValue = "-", Description="Format to export.")] // TODO Help text for available export formats
        public string ExportFormat { get; set; }
    }
}

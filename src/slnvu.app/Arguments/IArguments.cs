namespace slnvu.app.Arguments
{
    public interface IArguments
    {
        string InputFile { get; }

        string OutputFile { get; }

        string ExportFormat { get; }
    }
}

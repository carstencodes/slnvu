using System;
using NClap;
using NClap.Help;

namespace slnvu.app.Arguments
{
    internal sealed class NclapArgumentParser : IArgumentParser
    {
        public IArguments Parse(string[] arguments)
        {
            CommandLineParserOptions options = new CommandLineParserOptions();
            options.DisplayUsageInfoOnError = true;
            options.HelpOptions = new ArgumentSetHelpOptions();
            options.Reporter = (error) => Console.Error.WriteLine(error);
            if (CommandLineParser.TryParse(arguments, options, out NClapArguments commands))
            {
                return commands;
            }

            return default;
        }
    }
}

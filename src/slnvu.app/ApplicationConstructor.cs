using slnvu.app.Arguments;
using slnvu.app.IO;
using slnvu.app.Common;
using slnvu.core.Factory;

namespace slnvu.app
{
    public sealed class ApplicationConstructor : IObjectConstructor
    {
        public void AnnounceAvailableObjects(IObjectRegistry registry)
        {
            registry?.RegisterObject<IStreamFactory, ConsoleStreamFactory>();
            registry?.RegisterObject<IArgumentParser, NclapArgumentParser>();
            registry?.RegisterObject<IApplication, ConsoleApplication>();
        }
    }
}

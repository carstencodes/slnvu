using System;
using System.IO;

using slnvu.app.Common;

namespace slnvu.app.IO
{
    internal sealed class ConsoleStreamFactory : IStreamFactory
    {
        public Stream OpenInputStream(string inputFile)
        {
            if (string.IsNullOrWhiteSpace(inputFile))
            {
                throw new ArgumentNullException(nameof(inputFile));
            }

            if (StringComparer.InvariantCultureIgnoreCase.Equals(ConsoleInternals.StandardInputAsFile, inputFile))
            {
                return Console.OpenStandardInput();
            }

            return this.OpenExistingFile(inputFile);
        }

        public Stream OpenOutputStream(string outputFile)
        {
            if (string.IsNullOrWhiteSpace(outputFile))
            {
                throw new ArgumentNullException(nameof(outputFile));
            }

            if (StringComparer.InvariantCultureIgnoreCase.Equals(ConsoleInternals.StandardOutputAsFile, outputFile))
            {
                return Console.OpenStandardOutput();
            }

            return this.CreateNewFileOrRecreateExistingFile(outputFile);
        }

        private Stream CreateNewFileOrRecreateExistingFile(string outputFile)
        {
            if (File.Exists(outputFile))
            {
                return this.RecreateExistingFile(outputFile);
            }

            return this.CreateNewFile(outputFile);
        }

        private Stream CreateNewFile(string outputFile)
        {
            return File.Create(outputFile);
        }

        private Stream RecreateExistingFile(string outputFile)
        {
            this.DeleteExistingFile(outputFile);
            return this.CreateNewFile(outputFile);
        }

        private void DeleteExistingFile(string outputFile)
        {
            File.Delete(outputFile);
        }

        private Stream OpenExistingFile(string inputFile)
        {
            return File.OpenRead(inputFile);
        }
    }
}

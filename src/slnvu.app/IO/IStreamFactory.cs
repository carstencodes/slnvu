using System.IO;

namespace slnvu.app.IO
{
    public interface IStreamFactory
    {
        Stream OpenInputStream(string inputFile);
        Stream OpenOutputStream(string outputFile);
    }
}

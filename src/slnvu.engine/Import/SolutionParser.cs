using System;
using System.Collections.Generic;
using System.IO;
using net.r_eg.MvsSln;
using net.r_eg.MvsSln.Core;
using slnvu.engine.Model;

namespace slnvu.engine.Import
{
    public class SolutionParser : ISolutionParser
    {
        private readonly ISlnContainer parserEngine;

        public SolutionParser(ISlnContainer parserEngine)
        {
            this.parserEngine = parserEngine ?? throw new ArgumentNullException(nameof(parserEngine));
        }

        public SolutionParser() : this (new net.r_eg.MvsSln.Core.SlnParser())
        {
        }

        public ISolution Parse(Stream input)
        {
            SlnItems parsingOptions = SlnItems.Header | SlnItems.SolutionConfPlatforms | SlnItems.Projects | SlnItems.ProjectConfPlatforms;
            using (StreamReader inputReader = new StreamReader(input))
            {
                ISlnResult result = this.parserEngine.Parse(inputReader, parsingOptions);
                string fullFilePath = GetFullFilePathFromStream(input);

                return CreateSolutionParseResult(fullFilePath, result);
            }
        }

        private static string GetFullFilePathFromStream(Stream input)
        {
            const string StdIn = "-";

            if (input is FileStream fileStream)
            {
                string fileName = fileStream.Name;
                return Path.GetFullPath(fileName);
            }

            return StdIn;
        }

        private static ISolution CreateSolutionParseResult(string fullFilePath, ISlnResult result)
        {
            if (result != null)
            {
                Solution solution = new Solution()
                {
                    FileName = fullFilePath
                };

                solution.FormatVersion = result.Header.FormatVersion.ToString(); // TODO version formatter
                
                IReadOnlyDictionary<IConfPlatform, SolutionBuildConfiguration> configurationMappings = CreateSolutionBuildConfigurations(solution, result);

                IReadOnlyDictionary<ProjectItem, SolutionBuildProject> projectMappings = CreateSolutionBuildProjects(solution, result);

                MapSolutionConfigurationsToProject(result, configurationMappings, projectMappings);

                return solution;
            }

            return default;
        }

        private static void MapSolutionConfigurationsToProject(ISlnResult result, IReadOnlyDictionary<IConfPlatform, SolutionBuildConfiguration> configurationMappings, IReadOnlyDictionary<ProjectItem, SolutionBuildProject> projectMappings)
        {
            foreach (ProjectItemCfg projectConfiguration in result.ProjectItemsConfigs)
            {
                ProjectItem project = projectConfiguration.project;
                IConfPlatform solutionConfiguration = projectConfiguration.solutionConfig;
                if (projectMappings.TryGetValue(project, out SolutionBuildProject solutionBuildProject) 
                    && (solutionBuildProject != null))
                {
                    IConfPlatformPrj projectBuildConfiguration = projectConfiguration.projectConfig;
                    ProjectBuildConfiguration buildConfiguration = CreateProjectBuildConfiguration(projectBuildConfiguration);

                    if (configurationMappings.TryGetValue(solutionConfiguration, out SolutionBuildConfiguration solutionBuildConfiguration) 
                        && (solutionBuildConfiguration != null))
                    {
                        solutionBuildProject.MapBuildConfiguration(buildConfiguration, solutionBuildConfiguration);
                    }
                    else 
                    {
                        solutionBuildProject.AddUnmappedBuildConfiguration(buildConfiguration);
                    }
                }
            }
        }

        private static IReadOnlyDictionary<IConfPlatform, SolutionBuildConfiguration> CreateSolutionBuildConfigurations(Solution solution, ISlnResult result)
        {
            Dictionary<IConfPlatform, SolutionBuildConfiguration> configurationMappings = new Dictionary<IConfPlatform, SolutionBuildConfiguration>();
            foreach (IConfPlatform configuration in result.SolutionConfigs)
            {
                SolutionBuildConfiguration slnConfiguration = CreateSolutionBuildConfiguration(configuration);
                if (solution.AddConfiguration(slnConfiguration))
                {
                    configurationMappings.Add(configuration, slnConfiguration);
                }
            }

            return configurationMappings;
        }

        private static IReadOnlyDictionary<ProjectItem, SolutionBuildProject> CreateSolutionBuildProjects(Solution solution, ISlnResult result)
        {
            Dictionary<ProjectItem, SolutionBuildProject> projectMappings = new Dictionary<ProjectItem, SolutionBuildProject>();

            foreach (ProjectItem project in result.ProjectItems)
            {
                SolutionBuildProject slnProject = CreateSolutionBuildProject(project);
                if (solution.AddProject(slnProject))
                {
                    projectMappings.Add(project, slnProject);
                }
            }

            return projectMappings;
        }

        private static ProjectBuildConfiguration CreateProjectBuildConfiguration(IConfPlatformPrj projectBuildConfiguration)
        {
            if (null != projectBuildConfiguration)
            {
                ProjectBuildConfiguration configuration = new ProjectBuildConfiguration();
                configuration.ConfigurationName = projectBuildConfiguration.Configuration;
                configuration.PlatformName = projectBuildConfiguration.Platform;
                configuration.IsBuilt = projectBuildConfiguration.IncludeInBuild;
                configuration.IsDeployed = projectBuildConfiguration.IncludeInDeploy;
                return configuration;
            }

            return default;
        }

        private static SolutionBuildProject CreateSolutionBuildProject(ProjectItem project)
        {
            SolutionBuildProject buildProject = new SolutionBuildProject();
            buildProject.Name = project.name;
            buildProject.FilePath = Path.GetFullPath(project.fullPath);

            return buildProject;
        }

        private static SolutionBuildConfiguration CreateSolutionBuildConfiguration(IConfPlatform configuration)
        {
            if (null != configuration)
            {
                SolutionBuildConfiguration buildConfiguration = new SolutionBuildConfiguration();
                buildConfiguration.ConfigurationName = configuration.Configuration;
                buildConfiguration.PlatformName = configuration.Platform;

                return buildConfiguration;
            }

            return default;
        }
    }
}
using System.IO;
using slnvu.engine.Model;

namespace slnvu.engine.Import
{
    public interface ISolutionParser
    {
        ISolution Parse(Stream input);
    }
}
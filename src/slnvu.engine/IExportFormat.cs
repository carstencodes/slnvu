namespace slnvu.engine
{
    public interface IExportFormat
    {
         string Name { get; }
         string Description { get; }
    }
}

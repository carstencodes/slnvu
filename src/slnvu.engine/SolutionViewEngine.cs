using System.Collections.Generic;
using System.IO;
using slnvu.engine.Export;
using slnvu.engine.Import;
using slnvu.engine.Model;

namespace slnvu.engine
{
    public sealed class SolutionViewEngine : ISolutionViewEngine
    {
        private readonly ISolutionParser parser;
        private readonly IExportEngineFactory engineFactory;

        public SolutionViewEngine(ISolutionParser parser, IExportEngineFactory engineFactory)
        {
            this.parser = parser;
            this.engineFactory = engineFactory;
        }

        public void ExportSolution(Stream solutionInput, string type, Stream output)
        {
            ISolution solution = this.parser.Parse(solutionInput);
            IExportEngine export = this.engineFactory.CreateEngineByFileType(type);
            export.WriteOutput(solution, output);
        }

        public IReadOnlyCollection<IExportFormat> GetAvailableExportFormats()
        {
            List<IExportFormat> formats = new List<IExportFormat>();
            foreach ((string name, string description) in this.engineFactory.GetAvailableExportFormats())
            {
                formats.Add(new ExportFormat(name, description));
            }

            return formats.AsReadOnly();
        }
    }
}

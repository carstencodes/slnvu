using System;
using System.Collections.Generic;

namespace slnvu.engine.Model
{
    internal sealed class SolutionBuildProject : ISolutionBuildProject
    {
        private readonly Dictionary<IProjectBuildConfiguration, ISolutionBuildConfiguration> configurations = new Dictionary<IProjectBuildConfiguration, ISolutionBuildConfiguration>();

        public string Name { get; set; }

        public string FilePath { get; set; }

        public IReadOnlyDictionary<IProjectBuildConfiguration, ISolutionBuildConfiguration> Configurations => this.configurations;

        internal void AddUnmappedBuildConfiguration(IProjectBuildConfiguration buildConfiguration)
        {
            ISolutionBuildConfiguration configuration = new UnmappedSolutionBuildConfiguration();
            this.MapBuildConfiguration(buildConfiguration, configuration);
        }

        internal void MapBuildConfiguration(IProjectBuildConfiguration buildConfiguration, ISolutionBuildConfiguration solutionBuildConfiguration)
        {
            if (null != buildConfiguration && null != solutionBuildConfiguration)
            {
                this.configurations.Add(buildConfiguration, solutionBuildConfiguration);
            }
        }
    }
}
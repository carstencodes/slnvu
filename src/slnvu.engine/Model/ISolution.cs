using System.Collections.Generic;

namespace slnvu.engine.Model
{
    public interface ISolution
    {
        string FileName { get; }

        string FormatVersion { get; }

        IReadOnlyCollection<ISolutionBuildConfiguration> Configurations { get; }

        IReadOnlyCollection<ISolutionBuildProject> Projects { get; }
    }
}
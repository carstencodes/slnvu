using System.Collections.Generic;

namespace slnvu.engine.Model
{
    public interface ISolutionBuildProject
    {
        string Name { get; }

        string FilePath { get; }

        IReadOnlyDictionary<IProjectBuildConfiguration, ISolutionBuildConfiguration> Configurations { get; }
    }
}
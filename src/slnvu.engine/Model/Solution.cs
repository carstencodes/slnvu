using System.Collections.Generic;

namespace slnvu.engine.Model
{
    internal sealed class Solution : ISolution
    {
        private readonly List<ISolutionBuildConfiguration> configurations = new List<ISolutionBuildConfiguration>();

        private readonly List<ISolutionBuildProject> projects = new List<ISolutionBuildProject>();

        public string FileName { get; set; }
        public string FormatVersion { get; set; }

        public IReadOnlyCollection<ISolutionBuildConfiguration> Configurations => this.configurations;

        public IReadOnlyCollection<ISolutionBuildProject> Projects => this.projects;

        internal bool AddConfiguration(ISolutionBuildConfiguration configuration)
        {
            if (null != configuration)
            {
                this.configurations.Add(configuration);
                return true;
            }

            return false;
        }

        internal bool AddProject(ISolutionBuildProject project)
        {
            if (null != project)
            {
                this.projects.Add(project);
                return true;
            }

            return false;
        }
    }
}
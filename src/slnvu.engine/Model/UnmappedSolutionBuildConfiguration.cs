namespace slnvu.engine.Model
{
    internal class UnmappedSolutionBuildConfiguration : ISolutionBuildConfiguration
    {
        public string Name => "Unmapped";

        public string ConfigurationName => "Unmapped";

        public string PlatformName => "Unmapped";
    }
}
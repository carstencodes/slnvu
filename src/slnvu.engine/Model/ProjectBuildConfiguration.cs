namespace slnvu.engine.Model
{
    internal sealed class ProjectBuildConfiguration : IProjectBuildConfiguration
    {
        public string Name => $"{this.ConfigurationName}|{this.PlatformName}";

        public string ConfigurationName { get; set; }

        public string PlatformName { get; set; }

        public bool IsDeployed { get; set; }

        public bool IsBuilt { get; set; }
    }
}
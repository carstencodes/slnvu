namespace slnvu.engine.Model
{
    public interface IProjectBuildConfiguration
    {
        string Name { get; }

        string ConfigurationName { get; }

        string PlatformName { get; } 

        bool IsDeployed { get; }

        bool IsBuilt { get; }
    }
}
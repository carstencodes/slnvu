namespace slnvu.engine.Model
{
    internal sealed class SolutionBuildConfiguration : ISolutionBuildConfiguration
    {
        public string Name => $"{this.ConfigurationName}|{this.PlatformName}";

        public string ConfigurationName { get; set; }

        public string PlatformName { get; set; }
    }
}
namespace slnvu.engine.Model
{
    public interface ISolutionBuildConfiguration
    {
        string Name { get; }

        string ConfigurationName { get; }

        string PlatformName { get; } 
    }
}
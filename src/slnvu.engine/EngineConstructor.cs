using slnvu.core.Factory;
using slnvu.engine.Export;
using slnvu.engine.Export.ExcelFiles;
using slnvu.engine.Export.HtmlFiles;
using slnvu.engine.Export.JsonFiles;
using slnvu.engine.Import;

namespace slnvu.engine
{
    public sealed class EngineConstructor : IObjectConstructor
    {
        public void AnnounceAvailableObjects(IObjectRegistry registry)
        {
            registry?.RegisterObject<ISolutionParser, SolutionParser>();

            registry?.RegisterObject<IExcelFormatter, EpplusExcelFormatter>();
            registry?.RegisterObject<IHtmlFormatter, NVelocityHtmlFormatter>();
            registry?.RegisterObject<IJsonFormatter, NewtonsoftJsonFormatter>();

            registry?.RegisterObject<IExportEngineFactory, ExportEngineFactory>();
            registry?.RegisterObjectsFromAssembly<IExportEngine>();

            registry?.RegisterObject<ISolutionViewEngine, SolutionViewEngine>();
        }
    }
}

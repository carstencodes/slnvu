using System.Collections.Generic;
using System.IO;

namespace slnvu.engine
{
    public interface ISolutionViewEngine
    {
        void ExportSolution(Stream solution, string type, Stream output);

        IReadOnlyCollection<IExportFormat> GetAvailableExportFormats();
    }
}

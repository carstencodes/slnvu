using System;

namespace slnvu.engine
{
    internal sealed class ExportFormat : IExportFormat
    {
        public ExportFormat(string name, string description)
        {
            if (string.IsNullOrWhiteSpace(description))
            {
                throw new ArgumentNullException(nameof(name));
            }

            this.Name = name;
            this.Description = description ?? string.Empty;
        }

        public string Name { get; }

        public string Description { get; }
    }
}

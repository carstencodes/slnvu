using System;
using System.Collections;
using System.Collections.Generic;

namespace slnvu.engine.Export.View
{
    internal sealed class TabularConfigurationView : IReadOnlyCollection<TabularConfigurationRow>
    {
        private readonly IReadOnlyCollection<TabularConfigurationRow> rows;

        internal TabularConfigurationView(IReadOnlyCollection<string> header, IReadOnlyCollection<TabularConfigurationRow> rows)
        {
            this.TableHeader = header ?? throw new ArgumentNullException(nameof(header));
            this.rows = rows ?? throw new ArgumentNullException(nameof(rows));
        }

        internal static TabularConfigurationView Empty { get; } = new TabularConfigurationView(new List<string>(), new List<TabularConfigurationRow>());

        public IReadOnlyCollection<string> TableHeader { get; }

        public int Count => this.rows.Count;

        public IEnumerator<TabularConfigurationRow> GetEnumerator()
        {
            return this.rows.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
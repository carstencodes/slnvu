using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace slnvu.engine.Export.View
{
    internal struct TabularConfigurationRow : IReadOnlyCollection<ConfigurationCell>
    {
        private readonly IReadOnlyCollection<ConfigurationCell> cells;

        internal TabularConfigurationRow(string title, IReadOnlyCollection<ConfigurationCell> cells)
        {
            this.Title = title;
            this.cells = cells;
        }

        public string Title { get; }

        public int Count => this.cells?.Count ?? 0;

        public IEnumerator<ConfigurationCell> GetEnumerator()
        {
            return this.cells?.GetEnumerator() ?? Enumerable.Empty<ConfigurationCell>().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
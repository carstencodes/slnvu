namespace slnvu.engine.Export.View
{
    internal struct ConfigurationCell
    {
        public string SolutionConfigurationName { get; }

        public string ProjectConfigurationName { get; }

        internal ConfigurationCell(string solutionConfigurationName, string projectConfigurationName)
        {
            this.SolutionConfigurationName = solutionConfigurationName;
            this.ProjectConfigurationName = projectConfigurationName;
        }
    }
}
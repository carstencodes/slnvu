using System;
using System.IO;
using OfficeOpenXml;

namespace slnvu.engine.Export.ExcelFiles
{
    internal sealed class EpplusExcelFormatter : IExcelFormatter
    {
        private const int FirstRowIndex = 2;

        private const int FirstColumnIndex = 1;

        private ExcelPackage package = new ExcelPackage();

        private ExcelWorksheet worksheet = null;

        private int maxColumn = FirstColumnIndex;

        private int currentRowNumber = FirstRowIndex;

        // TODO Style it

        public void AddNewWorkSheet(string name)
        {
            this.CheckDisposed();
            ExcelWorkbook workbook = this.package.Workbook;
            this.worksheet = workbook.Worksheets.Add(name);
            this.maxColumn = FirstColumnIndex;
            this.currentRowNumber = FirstRowIndex;
        }

        public void AddTableRow(string[] content)
        {
            if (null == content)
            {
                throw new ArgumentNullException(nameof(content));
            }


            this.CheckDisposed();
            this.CheckWorkbook();
            for (int i = 0; i < content.LongLength; i ++)
            {
                int columnIndex = i + 1;
                this.worksheet.Cells[currentRowNumber, columnIndex].Value = content[i];
            }

            this.currentRowNumber++;
            this.UpdateColumnCount(content.Length);
        }

        public void AutoFormat()
        {
            const int FirstColumnAndRowIndex = 1;
            this.CheckDisposed();
            this.CheckWorkbook();

            int maximumColumnIndex = this.maxColumn + 1;
            this.worksheet.Cells[FirstColumnAndRowIndex, FirstColumnAndRowIndex, this.currentRowNumber, maximumColumnIndex].AutoFitColumns();
        }

        public void SendToFileStream(Stream targetStream)
        {
            this.CheckDisposed();
            this.package.SaveAs(targetStream);
        }

        public void SetTableHeader(string[] content)
        {
            const int FirstRowIndex = 1;
            if (null == content)
            {
                throw new ArgumentNullException(nameof(content));
            }

            this.CheckDisposed();
            this.CheckWorkbook();
            for (int i = 0; i < content.LongLength; i ++)
            {
                int column = i + 1;
                this.worksheet.Cells[FirstRowIndex, column].Value = content[i];
            }

            this.UpdateColumnCount(content.Length);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    this.package.Dispose();
                    this.package = null;
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }

        #endregion

        private void CheckDisposed()
        {
            if (this.disposedValue)
            {
                throw new ObjectDisposedException(nameof(EpplusExcelFormatter));
            }
        }

        private void CheckWorkbook()
        {
            if (null == this.worksheet)
            {
                throw new InvalidOperationException("No worksheet is active.");
            }
        }

        private void UpdateColumnCount(int length)
        {
            if (this.maxColumn < length)
            {
                this.maxColumn = length;
            }
        }
    }
}

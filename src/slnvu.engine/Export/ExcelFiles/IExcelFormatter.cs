using System;
using System.IO;

namespace slnvu.engine.Export.ExcelFiles
{
    public interface IExcelFormatter : IDisposable
    {
        void AddNewWorkSheet(string name);

        void AddTableRow(string[] content);

        void SetTableHeader(string[] content);

        void AutoFormat();

        void SendToFileStream(Stream targetStream);
    }
}
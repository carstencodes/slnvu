using slnvu.core.Factory;

namespace slnvu.engine.Export
{
    [StringMetaData("csv", Description = "Comma separeted values (CSV) - defaults to 'scsv'.")]
    [StringMetaData("scsv", Description = "Comma separeted values (CSV) using semicolons as separating keys.")]
    internal sealed class SemicolonSeparatedExportEngine : SeparatedValuesExportEngineBase
    {
        private static readonly char SeparateBySemicolons = ';';

        public SemicolonSeparatedExportEngine() : base(SeparateBySemicolons)
        {
        }
    }
}

using System.IO;

namespace slnvu.engine.Export.HtmlFiles
{
    public interface IHtmlFormatter
    {
        string Title { get; set; }

        void SetTableHeader(string[] content);

        void AddRow(string[] content);

        void SendToFileStream(Stream targetStream, Stream templateFileStream);
    }
}
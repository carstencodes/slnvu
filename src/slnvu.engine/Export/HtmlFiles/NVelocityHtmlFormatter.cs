using System;
using System.IO;
using NVelocity;
using NVelocity.App;
using Commons.Collections;
using System.Collections.Generic;

namespace slnvu.engine.Export.HtmlFiles
{
    public class NVelocityHtmlFormatter : IHtmlFormatter
    {
        private readonly List<string[]> rows = new List<string[]>();

        private string[] header = Array.Empty<string>();

        private VelocityContext context = new VelocityContext();

        private VelocityEngine engine = new VelocityEngine();

        public string Title { get; set; } = "Unknown";

        public void SetTableHeader(string[] content)
        {
            if (null == content)
            {
                throw new ArgumentNullException(nameof(content));
            }

            this.header = content;
        }

        public void AddRow(string[] content)
        {
            if (null == content)
            {
                throw new ArgumentNullException(nameof(content));
            }

            this.rows.Add(content);
        }

        public void SendToFileStream(Stream targetStream, Stream templateFileStream)
        {
            this.BuildContext();
            ExtendedProperties properties = this.CreateProperties();

            this.engine.Init(properties);

            using (StreamWriter writer = new StreamWriter(targetStream))
            {
                using (StreamReader template = new StreamReader(templateFileStream))
                {
                    engine.Evaluate(this.context, writer, this.Title, template);
                }
            }
        }

        private ExtendedProperties CreateProperties()
        {
            ExtendedProperties properties = new ExtendedProperties();
            return properties;
        }

        private void BuildContext()
        {
            this.context.Put(nameof(this.Title).ToLowerInvariant(), this.Title);
            this.context.Put(nameof(this.header), this.header);
            this.context.Put(nameof(this.rows), this.rows.ToArray());
        }
    }
}
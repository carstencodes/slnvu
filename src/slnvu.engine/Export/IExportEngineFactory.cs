using System;
using System.Collections.Generic;

namespace slnvu.engine.Export
{
    public interface IExportEngineFactory
    {
        IExportEngine CreateEngineByFileType(string extensionWithoutDot);
        IEnumerable<ValueTuple<string, string>> GetAvailableExportFormats();
    }
}

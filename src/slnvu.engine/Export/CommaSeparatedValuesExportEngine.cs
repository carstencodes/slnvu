using slnvu.core.Factory;

namespace slnvu.engine.Export
{
    [StringMetaData("ccsv", Description = "Comma separeted values (CSV) using commas as separating keys.")]
    internal sealed class CommaSeparatedValuesExportEngine : SeparatedValuesExportEngineBase
    {
        private static readonly char SeparateByCommas = ',';

        public CommaSeparatedValuesExportEngine() : base(SeparateByCommas)
        {
        }
    }
}

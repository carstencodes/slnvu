using System.IO;
using slnvu.engine.Model;

namespace slnvu.engine.Export
{
    public interface IExportEngine
    {
        void WriteOutput(ISolution solution, Stream output);
    }
}
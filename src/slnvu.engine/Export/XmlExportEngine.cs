using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using slnvu.core.Factory;
using slnvu.engine.Model;

namespace slnvu.engine.Export
{
    [StringMetaData("xml", Description = "An XML document that represents a solution as a tree.")]
    internal sealed class XmlExportEngine : IExportEngine
    {
        public void WriteOutput(ISolution solution, Stream output)
        {
            // TODO use intermediate XmlFormatter
            XElement headerNode = new XElement("Properties",
                new XElement(nameof(solution.FileName), solution.FileName),
                new XElement(nameof(solution.FormatVersion), solution.FormatVersion)
            );

            XElement configurationsNode = CreateConfigurationsNode(solution);

            XElement projectsNode = CreateProjectsNode(solution);

            XElement solutionNode = new XElement("Solution", headerNode, configurationsNode, projectsNode);

            XDocument document = new XDocument(solutionNode);

            this.WriteDocumentToTargetStream(document, output);
        }

        private static XElement CreateProjectsNode(ISolution solution)
        {
            List<XElement> projects = new List<XElement>();

            foreach (ISolutionBuildProject project in solution.Projects)
            {
                XElement projectNode = CreateProjectNode(project);
                projects.Add(projectNode);
            }

            XElement projectsNode = new XElement("Projects", projects.ToArray());
            return projectsNode;
        }

        private static XElement CreateConfigurationsNode(ISolution solution)
        {
            List<XElement> configurations = new List<XElement>();

            foreach (ISolutionBuildConfiguration buildConfiguration in solution.Configurations)
            {
                XElement configuration = new XElement("Configuration", 
                    new XAttribute(nameof(buildConfiguration.Name), buildConfiguration.Name),
                    new XAttribute(nameof(buildConfiguration.PlatformName), buildConfiguration.PlatformName),
                    new XAttribute(nameof(buildConfiguration.ConfigurationName), buildConfiguration.ConfigurationName)
                );
                configurations.Add(configuration);
            }

            XElement configurationsNode = new XElement(nameof(solution.Configurations), configurations.ToArray());
            return configurationsNode;
        }

        private static XElement CreateProjectNode(ISolutionBuildProject project)
        {
            List<XElement> configurations = new List<XElement>();
            foreach (IProjectBuildConfiguration projectBuild in project.Configurations.Keys)
            {
                ISolutionBuildConfiguration solutionConfiguration = project.Configurations[projectBuild];
                XElement configuration = new XElement("Configuration",
                    new XAttribute(nameof(projectBuild.Name), projectBuild.Name),
                    new XAttribute(nameof(projectBuild.ConfigurationName), projectBuild.ConfigurationName),
                    new XAttribute(nameof(projectBuild.PlatformName), projectBuild.PlatformName),
                    new XElement("MappedSolutionConfiguration",
                        new XAttribute(nameof(solutionConfiguration.Name), solutionConfiguration.Name),
                        new XElement("BuildWithConfiguration", projectBuild.IsBuilt),
                        new XElement("DeployedWithConfiguration", projectBuild.IsDeployed)
                    )
                );

                configurations.Add(configuration);
            }

            XElement configurationsNode = new XElement("Configurations", configurations.ToArray());
            XElement element = new XElement("Project",
                new XAttribute(nameof(project.Name), project.Name),
                new XAttribute(nameof(project.FilePath), project.FilePath),
                configurationsNode
            );

            return element;
        }

        private void WriteDocumentToTargetStream(XDocument document, Stream stream)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.CloseOutput = true;
            settings.Encoding = Encoding.UTF8;
            settings.Indent = true;
            using (XmlWriter writer = XmlWriter.Create(stream, settings))
            {
                document.WriteTo(writer);
            }
        }
    }
}

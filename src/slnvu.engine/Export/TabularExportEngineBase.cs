using System.IO;
using slnvu.engine.Export.View;
using slnvu.engine.Model;

namespace slnvu.engine.Export
{
    internal abstract class TabularExportEngineBase : IExportEngine
    {
        public abstract void WriteOutput(ISolution solution, Stream output);

        protected TabularConfigurationView TransformSolutionConfigurationToTable(ISolution solution, string emptyEntry = "-")
        {
            TabularTransformationEngine transformationEngine = new TabularTransformationEngine();
            return transformationEngine.TransformSolution(solution, emptyEntry);
        }
    }
}
using System;
using System.Collections.Generic;
using System.IO;
using slnvu.core.Factory;
using slnvu.engine.Export.JsonFiles;
using slnvu.engine.Model;

namespace slnvu.engine.Export
{
    [StringMetaData("json", Description = "JavaScript Object Notation")]
    internal sealed class JsonExportEngine : IExportEngine
    {
        private readonly IJsonFormatter fileFormatter;

        public JsonExportEngine(IJsonFormatter fileFormatter)
        {
            this.fileFormatter = fileFormatter;
        }

        public void WriteOutput(ISolution solution, Stream output)
        {
            this.fileFormatter.InitializeObject(nameof(solution));

            Dictionary<string, IConvertible> header = new Dictionary<string, IConvertible>();
            header.Add(nameof(ISolution.FileName), solution.FileName);
            header.Add(nameof(ISolution.FormatVersion), solution.FormatVersion);

            this.fileFormatter.AddProperty(nameof(header), header);

            List<string> configurations = new List<string>();
            foreach (ISolutionBuildConfiguration configuration in solution.Configurations)
            {
                configurations.Add(configuration.Name);
            }

            this.fileFormatter.AddProperty(nameof(configurations), configurations.AsReadOnly());

            this.FormatProjects(solution);

            this.fileFormatter.FinalizeObject();

            this.fileFormatter.SendToFileStream(output);
        }

        private static IReadOnlyCollection<IReadOnlyDictionary<string, IConvertible>> CreateProjectConfigurations(ISolutionBuildProject project)
        {
            List<IReadOnlyDictionary<string, IConvertible>> result = new List<IReadOnlyDictionary<string, IConvertible>>();

            foreach (IProjectBuildConfiguration configuration in project.Configurations.Keys)
            {
                ISolutionBuildConfiguration solutionBuild = project.Configurations[configuration];
                Dictionary<string, IConvertible> values = new Dictionary<string, IConvertible>();

                values.Add(nameof(configuration.Name).ToLowerInvariant(), configuration.Name);
                values.Add(nameof(configuration.ConfigurationName).ToLowerInvariant(), configuration.Name);
                values.Add(nameof(configuration.PlatformName).ToLowerInvariant(), configuration.PlatformName);
                values.Add(nameof(solutionBuild), solutionBuild.Name);
                values.Add("built", configuration.IsBuilt);
                values.Add("deployed", configuration.IsDeployed);

                result.Add(values);
            }

            return result.AsReadOnly();
        }

        private void FormatProjects(ISolution solution)
        {            
            using (this.fileFormatter.OpenScopedObject("projects"))
            {
                foreach (ISolutionBuildProject project in solution.Projects)
                {
                    using (this.fileFormatter.OpenScopedObject(project.Name))
                    {
                        this.fileFormatter.AddProperty("fileName", project.FilePath);

                        IReadOnlyCollection<IReadOnlyDictionary<string, IConvertible>> configurations = CreateProjectConfigurations(project);
                        this.fileFormatter.AddArrayProperty("configurations", configurations);
                    }
                }
            }        
        }
    }
}

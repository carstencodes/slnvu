using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using slnvu.core.Factory;
using slnvu.engine.Export.ExcelFiles;
using slnvu.engine.Export.View;
using slnvu.engine.Model;

namespace slnvu.engine.Export
{
    [StringMetaData("xslx", Description = "Microsoft Excel (OpenXML) file using open packaging conventions")]
    internal sealed class ExcelExportEngine : TabularExportEngineBase
    {
        private readonly Func<IExcelFormatter> formatterFactory;

        public ExcelExportEngine(Func<IExcelFormatter> formatterFactory)
        {
            this.formatterFactory = formatterFactory;
        } 

        public override void WriteOutput(ISolution solution, Stream output)
        {
            string fileName = Path.GetFileName(solution.FileName);
            string slnName = $"{fileName} ({solution.FormatVersion})";
            TabularConfigurationView tabularView = this.TransformSolutionConfigurationToTable(solution);
            this.WriteOutputStream(slnName, tabularView, output);
        }

        private void WriteOutputStream(string slnName, TabularConfigurationView tabularView, Stream targetStream)
        {
            if (null == tabularView)
            {
                throw new ArgumentNullException(nameof(tabularView));
            }

            using (IExcelFormatter formatter = this.formatterFactory())
            {
                formatter.AddNewWorkSheet(slnName);
                string[] header = tabularView.TableHeader.ToArray();
                formatter.SetTableHeader(header);
                foreach (TabularConfigurationRow row in tabularView)
                {
                    List<string> content = new List<string>();
                    content.Add(row.Title);
                    content.AddRange(row.Select(cell => cell.ProjectConfigurationName));
                    string[] rowContent = content.ToArray();
                    formatter.AddTableRow(rowContent);
                }

                formatter.AutoFormat();
                formatter.SendToFileStream(targetStream);
            }
        }
    }
}

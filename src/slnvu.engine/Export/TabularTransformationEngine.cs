using System;
using System.Collections.Generic;
using System.Linq;
using slnvu.engine.Export.View;
using slnvu.engine.Model;

namespace slnvu.engine.Export
{
    internal sealed class TabularTransformationEngine
    {
        private readonly StringComparer comparer;

        internal TabularTransformationEngine() : this(StringComparer.InvariantCulture)
        {
        }

        internal TabularTransformationEngine(StringComparer comparer)
        {
            this.comparer = comparer ?? throw new ArgumentNullException(nameof(comparer));
        }

        internal TabularConfigurationView TransformSolution(ISolution solution, string emptyEntry = "-")
        {
            if (null == solution)
            {
                return TabularConfigurationView.Empty;
            }

            IReadOnlyCollection<ISolutionBuildConfiguration> solutionConfigurations = solution.Configurations.OrderBy(item => item.Name, this.comparer).ToList().AsReadOnly();
            IReadOnlyCollection<string> solutionConfigurationsAsString = solutionConfigurations.Select(item => item.Name).ToList().AsReadOnly();
            IReadOnlyDictionary<string, ISolutionBuildProject> projectsByName = solution.Projects.OrderBy(GetDisplayNameOfProject, this.comparer).ToDictionary(GetDisplayNameOfProject);

            List<TabularConfigurationRow> rows = new List<TabularConfigurationRow>();
            foreach (KeyValuePair<string, ISolutionBuildProject> buildProject in projectsByName)
            {
                string projectName = buildProject.Key;
                ISolutionBuildProject project = buildProject.Value;

                if ((null == project) || (null == project.Configurations)) 
                {
                    continue;
                }

                List<ConfigurationCell> cells = new List<ConfigurationCell>();
                foreach (ISolutionBuildConfiguration configuration in solutionConfigurations)
                {
                    IProjectBuildConfiguration projectBuildConfiguration = GetProjectBuildConfiguration(project, configuration);
                    if (default != projectBuildConfiguration && projectBuildConfiguration.IsBuilt)
                    {
                        ConfigurationCell newCell = new ConfigurationCell(configuration.Name, projectBuildConfiguration.Name);
                        cells.Add(newCell);
                    }
                    else
                    {
                        ConfigurationCell newCell = new ConfigurationCell(configuration.Name, emptyEntry);
                        cells.Add(newCell);
                    }
                }

                TabularConfigurationRow newEntry = new TabularConfigurationRow(projectName, cells);
                rows.Add(newEntry);
            }

            List<string> header = new List<string>();
            header.Add(nameof(ISolution.Projects));
            header.AddRange(solutionConfigurationsAsString);

            return new TabularConfigurationView(header, rows);
        }

        private static IProjectBuildConfiguration GetProjectBuildConfiguration(ISolutionBuildProject project, ISolutionBuildConfiguration configuration)
        {
            if (null == project || null == project.Configurations || null == configuration)
            {
                return default;
            }

            var availableProjectConfigurationTuples = project.Configurations.Where(item => item.Value == configuration);
            var availableProjectConfigurations = availableProjectConfigurationTuples.Select(item => item.Key);
            var distinctProjectConfigurations = availableProjectConfigurations.Distinct();
            return distinctProjectConfigurations.SingleOrDefault();
        }

        private static string GetDisplayNameOfProject(ISolutionBuildProject project)
        {
            return $"{project.Name} ({project.FilePath})";
        }
    }
}
using slnvu.core.Factory;

namespace slnvu.engine.Export
{
    [StringMetaData("txt", Description = "Textual representation - defaults to 'tsv'")]
    [StringMetaData("tsv", Description = "Tabular separeted values (TSV) - like csv, but using tabs.")]
    internal sealed class TabularSeparatedValuesExportEngine : SeparatedValuesExportEngineBase
    {
        private static readonly char SeparateByTabs = '\t';

        public TabularSeparatedValuesExportEngine() : base(SeparateByTabs)
        {
        }
    }
}

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using slnvu.core.Factory;
using slnvu.engine.Export.HtmlFiles;
using slnvu.engine.Export.View;
using slnvu.engine.Model;

namespace slnvu.engine.Export
{
    [StringMetaData("html", Description = "HTML page with tables")]
    [StringMetaData("htm", Description = "An alias for 'html'")]
    internal sealed class HtmlExportEngine : TabularExportEngineBase
    {
        private readonly IHtmlFormatter formatter;

        public HtmlExportEngine(IHtmlFormatter formatter)
        {
            this.formatter = formatter;
        }

        public override void WriteOutput(ISolution solution, Stream output)
        {
            string fileName = Path.GetFileName(solution.FileName);
            string slnName = $"{fileName} ({solution.FormatVersion})";
            TabularConfigurationView tabularView = this.TransformSolutionConfigurationToTable(solution);
            this.WriteOutputStream(slnName, tabularView, output);
        }

        private void WriteOutputStream(string slnName, TabularConfigurationView tabularView, Stream targetStream)
        {
            if (null == tabularView)
            {
                throw new ArgumentNullException(nameof(tabularView));
            }

            formatter.Title = slnName;
            string[] header = tabularView.TableHeader.ToArray();
            formatter.SetTableHeader(header);

            foreach (TabularConfigurationRow row in tabularView)
            {
                List<string> content = new List<string>();
                content.Add(row.Title);
                string[] rowContent = row.Select(item => item.ProjectConfigurationName).ToArray();
                content.AddRange(rowContent);
                rowContent = content.ToArray();
                formatter.AddRow(rowContent);
            }

            using (Stream templateStream = File.OpenRead("Template.html"))
            {
                formatter.SendToFileStream(targetStream, templateStream);
            }
        }
    }
}

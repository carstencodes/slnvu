using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using slnvu.core.Factory;
using slnvu.engine.Model;

namespace slnvu.engine.Export
{
    [StringMetaData("tree", Description = "A textual tree representation")]
    internal sealed class TextTreeExportEngine : IExportEngine
    {
        private const int LineLength = 98;

        private const int FirstColumnWidth = 25;

        private readonly string separator = new string('-', LineLength);
         
        public void WriteOutput(ISolution solution, Stream output)
        {
            if (null != solution)
            {
                //this.WriteSolutionHeader(solution);
                //this.WriteTableHeaderOfSolutionConfigurations(solution);

                using (TextWriter writer = new StreamWriter(output))
                {
                    this.WriteTreeOfProjectConfigurations(solution, writer);
                }
            }
        }

        private void WriteSolutionHeader(ISolution solution, TextWriter writer)
        {
            Console.WriteLine($"+{this.separator}+");
            string formattedLine;

            formattedLine = FormatLine("FilePath", FirstColumnWidth, solution.FileName);
            Console.WriteLine(formattedLine);
            formattedLine = FormatLine("Solution version", FirstColumnWidth, solution.FormatVersion);
            Console.WriteLine(formattedLine);
            Console.WriteLine($"+{this.separator}+");
        }

        private void WriteTableHeaderOfSolutionConfigurations(ISolution solution, TextWriter writer)
        {
            List<string> configurations = new List<string>();
            foreach (ISolutionBuildConfiguration configuration in solution.Configurations.OrderBy(item => item.Name))
            {
                configurations.Add(configuration.Name);
            }

            string[] configurationElements = configurations.ToArray();
            string formattedLine = FormatLine(" ", FirstColumnWidth, configurationElements);
            Console.WriteLine(formattedLine);
            Console.WriteLine($"+{this.separator}+");
        }

        private void WriteTreeOfProjectConfigurations(ISolution solution, TextWriter writer)
        {
            writer.WriteLine($"{solution.FileName} ({solution.FormatVersion})");

            foreach (ISolutionBuildProject project in solution.Projects)
            {
                writer.WriteLine($" |-- {project.Name} ({project.FilePath})");
                foreach (IProjectBuildConfiguration configuration in project.Configurations.Keys.OrderBy(item => item.Name))
                {
                    if (configuration.IsBuilt)
                    {
                        writer.WriteLine($"        | -- {configuration.Name} -> {project.Configurations[configuration].Name} of solution");
                    }
                    else
                    {
                        writer.WriteLine($"        | -- {configuration.Name} -> not build in solution");
                    }
                }
            }
        }

        private static string FormatLine(string prefix, int length, params string[] elements)
        {
            string format = $"{{0, {length} }}";
            StringBuilder builder = new StringBuilder();

            builder.AppendFormat(format, prefix);
            string elementString = string.Join(" | ", elements);
            builder.Append(elementString);

            builder.Insert(0, "| ");
            int lineLeftLength = LineLength - builder.Length - 2;
            builder.Append(new string(' ', lineLeftLength));
            builder.Append(" |");

            return builder.ToString();
        }
    }
}

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Newtonsoft.Json;

namespace slnvu.engine.Export.JsonFiles
{
    internal sealed class NewtonsoftJsonFormatter : IJsonFormatter
    {
        private sealed class ScopedJsonObject : IDisposable
        {
            #region IDisposable Support
            private bool disposedValue = false; // To detect redundant calls

            private readonly JsonWriter writer;

            public ScopedJsonObject(JsonWriter writer)
            {
                this.writer = writer;
            }

            void Dispose(bool disposing)
            {
                if (!disposedValue)
                {
                    if (disposing)
                    {
                        this.writer.WriteEndObject();
                    }

                    disposedValue = true;
                }
            }
            public void Dispose()
            {
                Dispose(true);
            }
            #endregion

        }

        private readonly MemoryStream memoryStream;

        private readonly TextWriter textWriter;

        private readonly JsonWriter jsonWriter;

        public NewtonsoftJsonFormatter()
        {
            this.memoryStream = new MemoryStream();
            this.textWriter = new StreamWriter(this.memoryStream);
            this.jsonWriter = new JsonTextWriter(this.textWriter);
            this.jsonWriter.Formatting = Formatting.Indented;
        }

        public void AddArrayProperty(string propertyName, IReadOnlyCollection<IReadOnlyDictionary<string, IConvertible>> properties)
        {
            this.jsonWriter.WritePropertyName(propertyName);
            if (null != properties)
            {
                this.jsonWriter.WriteStartArray();
                foreach (IReadOnlyDictionary<string, IConvertible> singleProperty in properties)
                {
                    this.AddPropertyObject(singleProperty);
                }

                this.jsonWriter.WriteEndArray();
            }
            else
            {
                this.jsonWriter.WriteNull();
            }
        }

        public void AddProperty(string propertyName, IReadOnlyDictionary<string, IConvertible> property)
        {
            this.jsonWriter.WritePropertyName(propertyName);
            this.AddPropertyObject(property);
        }

        public void FinalizeObject()
        {
            this.jsonWriter.WriteEndObject();
        }

        public void InitializeObject(string objectName)
        {
            this.jsonWriter.Flush();
            this.textWriter.Flush();
            this.memoryStream.Seek(0, SeekOrigin.Begin);
            this.memoryStream.SetLength(0);
            this.jsonWriter.WriteComment(objectName);
            this.jsonWriter.WriteStartObject();
        }

        public IDisposable OpenScopedObject(string objectName)
        {
            this.jsonWriter.WritePropertyName(objectName);
            this.jsonWriter.WriteStartObject();
            return new ScopedJsonObject(this.jsonWriter);
        }

        public void SendToFileStream(Stream targetStream)
        {
            this.jsonWriter.Flush();
            this.textWriter.Flush();
            this.memoryStream.Seek(0, SeekOrigin.Begin);

            using (StreamWriter writer = new StreamWriter(targetStream))
            {
                this.memoryStream.CopyTo(targetStream, 4096);
            }
        }

        private void AddPropertyObject(IReadOnlyDictionary<string, IConvertible> property)
        {
            if (null != property)
            {
                this.jsonWriter.WriteStartObject();
                foreach (string propertyName in property.Keys)
                {
                    IConvertible value = property[propertyName];
                    this.jsonWriter.WritePropertyName(propertyName);
                    if (null != value)
                    {
                        this.jsonWriter.WriteValue(value);
                    }
                    else 
                    {
                        this.jsonWriter.WriteNull();
                    }
                }

                this.jsonWriter.WriteEndObject();
            }
            else
            {
                this.jsonWriter.WriteNull();
            }
        }

        public void AddProperty(string propertyName, IConvertible property)
        {
            this.jsonWriter.WritePropertyName(propertyName);
            this.jsonWriter.WriteValue(property);
        }

        public void AddProperty(string propertyName, IReadOnlyCollection<IConvertible> property)
        {
            this.jsonWriter.WritePropertyName(propertyName);
            this.jsonWriter.WriteStartArray();
            foreach (IConvertible value in property)
            {
                this.jsonWriter.WriteValue(value);
            }
            
            this.jsonWriter.WriteEndArray();
        }
    }
}

using System;
using System.Collections.Generic;
using System.IO;

namespace slnvu.engine.Export.JsonFiles
{
    public interface IJsonFormatter
    {
        void InitializeObject(string objectName);
        void AddProperty(string propertyName, IReadOnlyDictionary<string, IConvertible> property);
        void AddProperty(string propertyName, IConvertible property);
        void AddProperty(string propertyName, IReadOnlyCollection<IConvertible> property);
        void AddArrayProperty(string propertyName, IReadOnlyCollection<IReadOnlyDictionary<string, IConvertible>> properties);
        void SendToFileStream(Stream targetStream);
        void FinalizeObject();
        IDisposable OpenScopedObject(string objectName);
    }
}
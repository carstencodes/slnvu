using System;
using System.Collections.Generic;
using System.IO;
using slnvu.core.Factory;

namespace slnvu.engine.Export
{
    public class ExportEngineFactory : IExportEngineFactory
    {
        private readonly IStringParameterizedObjectFactory objectFactory;

        public ExportEngineFactory(IStringParameterizedObjectFactory objectFactory)
        {
            this.objectFactory = objectFactory;
        }

        public IExportEngine CreateEngineByFileType(string extensionWithoutDot)
        {
            if (string.IsNullOrWhiteSpace(extensionWithoutDot))
            {
                throw new System.ArgumentNullException(nameof(extensionWithoutDot));
            }

            extensionWithoutDot = extensionWithoutDot.ToLowerInvariant().Trim();
            if (extensionWithoutDot != "-")
            {
                return this.objectFactory.Create<IExportEngine>(extensionWithoutDot);
            }

            return new TextTreeExportEngine();
        }

        IEnumerable<(string, string)> IExportEngineFactory.GetAvailableExportFormats()
        {
            foreach (string key in this.objectFactory.GetKeys<IExportEngine>())
            {
                yield return ( key, this.objectFactory.GetDescriptionForKey(key) );
            }
        }
    }
}

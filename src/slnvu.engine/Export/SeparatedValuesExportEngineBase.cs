using System;
using System.IO;
using System.Linq;
using slnvu.engine.Export.View;
using slnvu.engine.Model;

namespace slnvu.engine.Export
{
    internal abstract class SeparatedValuesExportEngineBase : TabularExportEngineBase
    {
        private readonly char separator;

        protected SeparatedValuesExportEngineBase(char separator)
        {
            this.separator = separator;
        }

        public sealed override void WriteOutput(ISolution solution, Stream output)
        {
            using (TextWriter textWriter = new StreamWriter(output))
            {
                TabularConfigurationView tabularView = this.TransformSolutionConfigurationToTable(solution);
                this.WriteOutputStream(tabularView, textWriter);
            }
        }

        private void WriteOutputStream(TabularConfigurationView tabularView, TextWriter textWriter)
        {
            if (null == tabularView)
            {
                throw new ArgumentNullException(nameof(tabularView));
            }

            // TODO add cell separating char (', ")
            string currentLine = string.Join(this.separator, tabularView.TableHeader);
            textWriter.WriteLine(currentLine);
            foreach (TabularConfigurationRow row in tabularView)
            {
                textWriter.Write(row.Title);
                textWriter.Write(this.separator);
                
                currentLine = string.Join(this.separator, row.Select(item => item.ProjectConfigurationName));
                textWriter.WriteLine(currentLine);
            }
        }
    }
}

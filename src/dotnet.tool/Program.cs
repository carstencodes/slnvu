﻿using slnvu.app;
using slnvu.app.IO;
using slnvu.core;
using slnvu.core.Factory;
using slnvu.engine;

namespace slnvu.dotnet.tool
{
    class Program
    {
        static int Main(string[] args)
        {
            IApplicationComposition composition = new ApplicationComposition();
            if (composition.Factory is IObjectRegistry registry)
            {
                registry.BeginInit();
                new ApplicationConstructor().AnnounceAvailableObjects(registry);
                new EngineConstructor().AnnounceAvailableObjects(registry);
                registry.RegisterObject<IStreamFactory, FileStreamFactory>();
                registry.EndInit();
            }

            IApplication application = composition.Factory.Create<IApplication>();
            return application.Run(args);
        }
    }
}

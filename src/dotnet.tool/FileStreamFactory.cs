using System;
using System.IO;

using slnvu.app.Common;
using slnvu.app.IO;

namespace slnvu.dotnet.tool
{
    internal sealed class FileStreamFactory : IStreamFactory
    {
        public Stream OpenInputStream(string inputFile)
        {
            if (string.IsNullOrWhiteSpace(inputFile))
            {
                throw new ArgumentNullException(nameof(inputFile));
            }

            return this.OpenExistingFile(inputFile);
        }

        public Stream OpenOutputStream(string outputFile)
        {
            if (string.IsNullOrWhiteSpace(outputFile))
            {
                throw new ArgumentNullException(nameof(outputFile));
            }
            
            return this.CreateNewFileOrRecreateExistingFile(outputFile);
        }

        private Stream CreateNewFileOrRecreateExistingFile(string outputFile)
        {
            if (File.Exists(outputFile))
            {
                return this.RecreateExistingFile(outputFile);
            }

            return this.CreateNewFile(outputFile);
        }

        private Stream CreateNewFile(string outputFile)
        {
            return File.Create(outputFile);
        }

        private Stream RecreateExistingFile(string outputFile)
        {
            this.DeleteExistingFile(outputFile);
            return this.CreateNewFile(outputFile);
        }

        private void DeleteExistingFile(string outputFile)
        {
            File.Delete(outputFile);
        }

        private Stream OpenExistingFile(string inputFile)
        {
            return File.OpenRead(inputFile);
        }
    }
}

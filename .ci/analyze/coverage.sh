#!/bin/sh

set -e

export PATH=$CI_PROJECT_DIR/tools:~/.dotnet/tools:$PATH

cd $CI_PROJECT_DIR/test/coverage
for file in *.coverage.xml; 
do 
    HTML_REPORT_DIR=$CI_PROJECT_DIR/analysis/coverage/${file/.coverage.xml/_html}
    test -d $HTML_REPORT_DIR || mkdir -p $HTML_REPORT_DIR
    $CI_PROJECT_DIR/tools/reportgenerator -reports:$file -targetdir:$HTML_REPORT_DIR
done

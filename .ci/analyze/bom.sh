#!/bin/sh

set -e

export PATH=$CI_PROJECT_DIR/tools:~/.dotnet/tools:$PATH

cd $CI_PROJECT_DIR/src
dotnet restore
dotnet project-licenses -i . | tee $CI_PROJECT_DIR/analysis/bom/bill-of-materials.txt
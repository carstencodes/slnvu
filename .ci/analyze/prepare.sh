#!/bin/sh

set -e

LOCAL_DIR=$CI_PROJECT_DIR/.ci
. $LOCAL_DIR/prepare.sh
LOCAL_DIR=$CI_PROJECT_DIR/.ci/analyze

for report in $@;
do
    mkdir -p $CI_PROJECT_DIR/analysis/$report
    if [ -x $LOCAL_DIR/prepare.$report.sh ]
    then
        . $LOCAL_DIR/prepare.$report.sh
    fi
done
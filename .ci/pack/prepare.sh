#!/bin/sh

set -e

apk add zip tar gzip curl

LOCAL_DIR=$CI_PROJECT_DIR/.ci
. $LOCAL_DIR/prepare.sh

test -z $1 || mkdir -p $CI_PROJECT_DIR/pack/$1
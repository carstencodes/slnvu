#!/bin/sh

set -e

LOCAL_DIR=$CI_PROJECT_DIR/.ci/pack

. $LOCAL_DIR/run.sh linux-x64

. $LOCAL_DIR/tgz.sh linux-x64
#!/bin/sh

set -e

RID=$1
test -z $RID && exit 1

cd $CI_PROJECT_DIR/pack/$RID

zip slnvu-$RID.zip * -x "*.pdb"

sha512sum -b slnvu-$RID.zip > slnvu-$RID.sha512 

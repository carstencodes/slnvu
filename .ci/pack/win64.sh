#!/bin/sh

set -e

LOCAL_DIR=$CI_PROJECT_DIR/.ci/pack

. $LOCAL_DIR/run.sh win10-x64

. $LOCAL_DIR/zip.sh win10-x64
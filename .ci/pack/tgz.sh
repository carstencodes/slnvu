#!/bin/sh

set -e

RID=$1
test -z $RID && exit 1

cd $CI_PROJECT_DIR/pack/$RID

tar --exclude='*.pdb' -zcvf slvnu-$RID.tgz $(ls *)

sha512sum -b slvnu-$RID.tgz > slvnu-$RID.sha512
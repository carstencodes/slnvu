#!/bin/sh

set -e

RID=$1
test -z $RID && exit 1

cd $CI_PROJECT_DIR/src

dotnet publish -c Release -f netcoreapp2.2 -r $RID --force -o $CI_PROJECT_DIR/pack/$RID --self-contained false slnvu.console/slnvu.console.csproj

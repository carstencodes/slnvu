#!/bin/sh

set -e

LOCAL_DIR=$CI_PROJECT_DIR/.ci/test
. $LOCAL_DIR/test.env.sh

test -d $CI_PROJECT_DIR/test/coverage || mkdir -p $CI_PROJECT_DIR/test/coverage

cd $CI_PROJECT_DIR/test
for test in $TEST_PROJECTS;
do
    cd $CI_PROJECT_DIR/test
    dotnet test $test/slnvu.$test.tests.csproj $RT_ARGS -p:AltCover=true 

    COVERAGE_FILE=$CI_PROJECT_DIR/test/$test/coverage.xml
    if [ -f $COVERAGE_FILE ];
    then 
        cp -v $COVERAGE_FILE $CI_PROJECT_DIR/test/coverage/$test.coverage.xml
    else
        echo Could not find coverage file for $test at $COVERAGE_FILE
    fi

    cd $CI_PROJECT_DIR
done

test $(find $CI_PROJECT_DIR/test/coverage -name *.coverage.xml | wc -l) -gt 0 || (
    echo No artifacts for code coverage found. 1>&2
    exit 1
)
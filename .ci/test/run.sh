#!/bin/sh

set -e

LOCAL_DIR=$CI_PROJECT_DIR/.ci/test
. $LOCAL_DIR/test.env.sh

test -d $CI_PROJECT_DIR/test/run || mkdir -p $CI_PROJECT_DIR/test/run

cd $CI_PROJECT_DIR/test

for test in $TEST_PROJECTS; 
do 
    dotnet test $test/slnvu.$test.tests.csproj $RT_ARGS -p:AltCover=false --test-adapter-path:. --logger:"nunit;LogFilePath=test.xml"

    if [ -f $test/test.xml ];
    then
        cp -v $test/test.xml $CI_PROJECT_DIR/test/run/$test.xml
    else
        echo Could not find expected artifact file for test $test at $test/test.xml
    fi;
done
#!/bin/sh

set -e

LOCAL_DIR=$CI_PROJECT_DIR/.ci
. $LOCAL_DIR/prepare.sh

test -z $1 || mkdir -p $CI_PROJECT_DIR/test/$1

#!/bin/sh

set -e

test -z $CI_PROJECT_DIR && exit 1
test -d $CI_PROJECT_DIR/log || mkdir $CI_PROJECT_DIR/log

#!/bin/sh

set -e

LOCAL_DIR=$CI_PROJECT_DIR/.ci/build
. $LOCAL_DIR/build.env.sh

cd $CI_PROJECT_DIR/test

dotnet build $BUILD_ARGS -c Release $@
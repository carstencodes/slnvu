#!/bin/sh

set -e

CONFIGURATION=$1
test -z $CONFIGURATION && exit 1

LOCAL_DIR=$CI_PROJECT_DIR/.ci/build
. $LOCAL_DIR/build.env.sh

cd $CI_PROJECT_DIR/src
dotnet build $BUILD_ARGS -c $CONFIGURATION -p:CiBuildRevision=$CI_PIPELINE_IID

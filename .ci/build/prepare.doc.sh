#!/bin/sh

set -e

apk add doxygen curl
curl https://raw.githubusercontent.com/nnen/doxygen-theme/master/metro/customdoxygen.css --output $CI_PROJECT_DIR/doc/code/customdoxygen.css
curl https://raw.githubusercontent.com/nnen/doxygen-theme/master/metro/DoxygenLayout.xml --output $CI_PROJECT_DIR/doc/code/DoxygenLayout.xml
#!/bin/sh

set -e

tar -zcvf /tmp/sources.tgz $CI_PROJECT_DIR
mv -v /tmp/sources.tgz $CI_PROJECT_DIR
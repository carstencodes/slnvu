# slnvu

## Introduction and purpose

slnvu (pronunciation: es-el-en-view) is a dotnet core bases application analyzing Microsoft Visual Studio solution files and exporting the structure of the solution as a tree based or tabular file structure making it more easy for humans to read and understand. On the other hand, also machine readable file formats are supported.

## Features

The following features are supported:

* read solution files and parsing solution files from the standard input
* exporting to files and standard output
* support of various file formats
    * csv
    * tsv
    * csv with semicolons
    * json
    * html (with customizable templates)
    * XML
    * Excel files
* Usage as stand-alone command line tool or a dotnet tool

## Usage

The tool is distributed as a separate framework dependent executable, i.e. the dotnet core framework must be installed, but you must not run the dotnet executable directly. It is called indirectly from the using the distributed executable.

### Basic command line usage

```shell
slvu [-f format] [-o output-file] input-file
```

According to unix conventions, the input file can be sent to stdin. If no output file is set or explicitly set to stdout by using `-o -`, the output will be written to stdout.

Please note, that the dotnet tool will only support file-based I/O, no stream based directions.

## Supported operating systems

Currently, publishing will be performed for the following operating systems:

* Windows x64
* Linux x64

The tests will be conducted on linux based operating system.

## Versioning

The main executable will use the following scheme: 

`YY.MM.DD.BI`

where 

* `YY` is the short year of the build
* `MM` is the month of the build
* `DD` is the day of the build
* `BI` is the incrementing build id

The core libraries are versioned using a semantic versioning in rev 2.0.0, though a fourth version number is used to identify the build.

## Downloading artifacts

You can download the released binaries from the bite-that-bit owncloud server accessible [here](https://cloud.bite-that-bit.de/index.php/s/x9N2aWKS6PIVhmi).

## Building from source

Enter the `src` folder and run the following command:

```shell
dotnet build -f netcoreapp2.2 -c Release
```
